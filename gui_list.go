package main

import (
	"path"
	"time"
)

func listPrevious() {
	if queueFocused {
		queuePrevious()
		return
	}

	if mainBufferOrigin > 0 && mainBufferCursor == mainBufferOrigin {
		mainBufferOrigin--
	}
	if mainBufferCursor > 0 {
		mainBufferCursor--
	}
	go app.QueueUpdateDraw(updateLists)
}

func listNext() {
	if queueFocused {
		queueNext()
		return
	}

	if mainBufferCursor < len(mainBufferFiles) {
		mainBufferCursor++
		if mainBufferCursor-mainBufferOrigin > mainBufHeight-3 {
			mainBufferOrigin++
		}
	}
	go app.QueueUpdateDraw(updateLists)
}

func queuePrevious() {
	if queueOrigin > 0 && queueCursor == queueOrigin {
		queueOrigin--
	}
	if queueCursor > 0 {
		queueCursor--
	}

	go app.QueueUpdateDraw(updateQueue)
}

func queueNext() {
	if queueCursor < len(queueFiles)-1 {
		queueCursor++
		queueOrigin = queueCursor - ((queueHeight - 3) / 2)
		if queueOrigin < 0 {
			queueOrigin = 0
		}
		if queueOrigin > (len(queueFiles)-1)-(queueHeight-3) {
			queueOrigin = (len(queueFiles) - 1) - (queueHeight - 3)
		}
	}

	go app.QueueUpdateDraw(updateQueue)
}

func listSelect() {
	if queueFocused {
		queueSelect()
		return
	}

	if mainBufferCursor == 0 {
		browseFolder(path.Join(mainBufferDirectory, ".."))
		return
	}

	entry := selectedMainEntry()
	if entry.File.IsDir() {
		browseFolder(entry.Path)
		return
	}

	audioFile, err := openFile(entry.Path, entry.Metadata)
	if err != nil {
		statusText = err.Error()
		go func() {
			time.Sleep(5 * time.Second)
			statusText = ""
			go app.QueueUpdateDraw(updateMain)
		}()
		go app.QueueUpdateDraw(updateMain)
		return
	}
	go play(audioFile)

	go app.QueueUpdateDraw(updateStatus)
}

func queueSelect() {
	entry := selectedQueueEntry()
	if entry.File.IsDir() {
		// TODO error
		return
	}

	audioFile, err := openFile(entry.Path, entry.Metadata)
	if err != nil {
		statusText = err.Error()
		go func() {
			time.Sleep(5 * time.Second)
			statusText = ""
			go app.QueueUpdateDraw(updateMain)
		}()
		go app.QueueUpdateDraw(updateMain)
		return
	}
	go play(audioFile)

	go app.QueueUpdateDraw(updateStatus)
}

func listQueue() {
	if mainBufferCursor == 0 {
		// TODO Show error
		return
	}

	entry := selectedMainEntry()
	if entry == nil {
		return
	} else if entry.File.IsDir() {
		scanFiles := scanFolderRecursively(entry.Path)

		queueLock.Lock()
		queueFiles = append(queueFiles, scanFiles...)
		queueLock.Unlock()

		if !disableAutoplay && playingFileID == 0 && len(queueFiles) > 0 {
			queueSelect()
		}
		go app.QueueUpdateDraw(updateQueue)
		return
	}

	queueLock.Lock()
	defer queueLock.Unlock()

	queueFiles = append(queueFiles, entry)

	if !disableAutoplay && playingFileID == 0 {
		queueSelect()
	}
	go app.QueueUpdateDraw(updateLists)
}

func listDelete() {
	if !queueFocused {
		return
	}

	queueLock.Lock()
	defer queueLock.Unlock()

	if len(queueFiles) <= queueCursor {
		return
	}

	queueFiles = append(queueFiles[:queueCursor], queueFiles[queueCursor+1:]...)

	if queueCursor > len(queueFiles)-1 {
		queueCursor = len(queueFiles) - 1
		if queueCursor < 0 {
			queueCursor = 0
		}
	}

	go app.QueueUpdateDraw(updateQueue)
}

func selectedMainEntry() *libraryEntry {
	return mainBufferFiles[mainBufferCursor-1]
}

func offsetMainEntry(offset int) *libraryEntry {
	if (mainBufferCursor-1)+offset < 0 || (mainBufferCursor-1)+offset >= len(mainBufferFiles) {
		return nil
	}
	return mainBufferFiles[(mainBufferCursor-1)+offset]
}

func selectedQueueEntry() *libraryEntry {
	return queueFiles[queueCursor]
}

func offsetQueueEntry(offset int) *libraryEntry {
	if queueCursor+offset < 0 || queueCursor+offset >= len(queueFiles) {
		return nil
	}
	return queueFiles[queueCursor+offset]
}

func listPreviousPage() {
	if !queueFocused {
		if mainBufferOrigin == 0 {
			mainBufferCursor = 0

			go app.QueueUpdateDraw(updateMain)
			return
		}

		mainBufferOrigin -= (mainBufHeight - 1) - 2
		if mainBufferOrigin < 0 {
			mainBufferOrigin = 0
		}
		mainBufferCursor = mainBufferOrigin

		go app.QueueUpdateDraw(updateMain)
	} else {
		if queueOrigin == 0 {
			queueCursor = 0

			go app.QueueUpdateDraw(updateQueue)
			return
		}

		queueOrigin -= (queueHeight - 1) - 2
		if queueOrigin < 0 {
			queueOrigin = 0
		}
		queueCursor = queueOrigin

		go app.QueueUpdateDraw(updateQueue)
	}
}

func listNextPage() {
	if !queueFocused {
		numEntries := len(mainBufferFiles)

		if mainBufferOrigin >= numEntries-(mainBufHeight-1) {
			mainBufferCursor = numEntries

			go app.QueueUpdateDraw(updateMain)
			return
		}

		mainBufferOrigin += (mainBufHeight - 1) - 2
		if mainBufferOrigin > (numEntries-(mainBufHeight-1))+2 {
			mainBufferOrigin = (numEntries - (mainBufHeight - 1)) + 2
		}
		mainBufferCursor = mainBufferOrigin

		go app.QueueUpdateDraw(updateMain)
	} else {
		numEntries := len(queueFiles)

		if queueOrigin >= numEntries-(queueHeight-1) {
			queueCursor = numEntries - 1

			go app.QueueUpdateDraw(updateQueue)
			return
		}

		queueOrigin += queueHeight - 2
		if queueOrigin > ((numEntries-1)-(queueHeight-1))+2 {
			queueOrigin = ((numEntries - 1) - (queueHeight - 1)) + 2
		}
		queueCursor = queueOrigin

		go app.QueueUpdateDraw(updateQueue)
	}
}

func listRefresh() {
	// TODO Remember cursor position
	d := mainBufferDirectory
	mainBufferDirectory = ""
	browseFolder(d)
}

func listToggleHidden() {
	showHiddenFolders = !showHiddenFolders

	if showHiddenFolders {
		statusText = "Hidden folders: shown"
	} else {
		statusText = "Hidden folders: hidden"
	}
	go func() {
		time.Sleep(3 * time.Second)
		statusText = ""
		go app.QueueUpdateDraw(updateMain)
	}()

	listRefresh()
}

func toggleFocusedList() {
	queueFocused = !queueFocused

	go app.QueueUpdateDraw(updateLists)
}
