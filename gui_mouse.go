package main

import (
	"path"
	"strings"
	"time"

	"github.com/faiface/beep/speaker"
	"github.com/gdamore/tcell"
	"gitlab.com/tslocum/cview"
)

func handleMouse(event *cview.EventMouse) *cview.EventMouse {
	if event.Action()&cview.MouseDown != 0 && event.Buttons()&tcell.Button1 != 0 {
		mouseX, mouseY := event.Position()
		if mouseY >= 0 && mouseY < mainBufHeight {
			if queueFocused {
				queueFocused = false
				go app.QueueUpdateDraw(updateLists)
			}

			// TODO Delay playing while cursor is moved
			if mouseY > 0 && mouseY < mainBufHeight-1 && mouseY-1 < len(mainBufferFiles)+1 {
				mainBufferCursor = mainBufferOrigin + (mouseY - 1)
				go app.QueueUpdateDraw(updateLists)
				go listSelect()
			}
			return nil
		} else if mouseY >= mainBufHeight && mouseY < screenHeight-2 {
			if !queueFocused {
				queueFocused = true
				go app.QueueUpdateDraw(updateLists)
			}

			if mouseY > mainBufHeight && mouseY < screenHeight-3 {
				mouseHit := (mouseY - mainBufHeight) - 1
				if mouseHit < len(queueFiles) {
					queueCursor = queueOrigin + mouseHit
					go app.QueueUpdateDraw(updateLists)
					go queueSelect()
				}
			}
			return nil
		} else if mouseY == screenHeight-1 {
			if mouseX >= seekStart && mouseX <= seekEnd {
				if strings.ToLower(path.Ext(playingFileName)) == ".flac" {
					statusText = "Seeking FLAC files is unsupported"
					go func() {
						time.Sleep(5 * time.Second)
						statusText = ""
						go app.QueueUpdateDraw(updateMain)
					}()
					go app.QueueUpdateDraw(updateMain)

					return nil
				}

				audioLock.Lock()
				speaker.Lock()

				if playingStreamer == nil {
					speaker.Unlock()
					audioLock.Unlock()
					return nil
				}

				pos := float64(mouseX-seekStart) / float64(seekEnd-seekStart)
				if pos > 1 {
					pos = 1
				}
				seekTo := int(float64(playingStreamer.Len()-1) * pos)
				_ = playingStreamer.Seek(seekTo) // Ignore seek errors
				speaker.Unlock()
				audioLock.Unlock()

				go app.QueueUpdateDraw(updateStatus)
				return nil
			} else if mouseX >= volumeStart && mouseX <= volumeEnd+1 {
				if mouseX > volumeEnd {
					mouseX = volumeEnd
				}

				if mouseX-volumeStart <= 3 {
					audioLock.Lock()
					speaker.Lock()

					if volume == nil {
						speaker.Unlock()
						audioLock.Unlock()
						return nil
					}

					volume.Silent = !volume.Silent
					speaker.Unlock()
					audioLock.Unlock()

					go app.QueueUpdateDraw(updateStatus)
				} else {
					vol := -7.5 + float64(7.5)*(float64(mouseX-volumeStart-3)/float64(volumeEnd-volumeStart-3))
					if vol < -7.0 {
						vol = -7.0
					}
					setVolume(roundUnit(vol, 0.5))

					go app.QueueUpdateDraw(updateStatus)
				}
				return nil
			}
		}
	}

	return event
}
