# ditty
[![GoDoc](https://godoc.org/gitlab.com/tslocum/ditty?status.svg)](https://godoc.org/gitlab.com/tslocum/ditty)
[![CI status](https://gitlab.com/tslocum/ditty/badges/master/pipeline.svg)](https://gitlab.com/tslocum/ditty/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Audio player

## Screenshot

[![](https://ditty.rocketnine.space/static/screenshot2.png)](https://ditty.rocketnine.space/static/screenshot2.png)

## Demo

If you are running Linux with ALSA, you can try ditty without installing:

```ssh ditty.rocketnine.space -p 20020 2> >(aplay --quiet)```

If you can't hear anything, you may need to specify which device ```aplay```
should use with ```-D```.

To list available sound devices, execute ```aplay -l```.

## Install

Choose one of the following methods:

### Download

[**Download ditty**](https://ditty.rocketnine.space/download/?sort=name&order=desc)

### Compile

```go get gitlab.com/tslocum/ditty```

## Dependencies

* [faiface/beep](https://github.com/faiface/beep)
* [hajimehoshi/oto](https://github.com/hajimehoshi/oto)
* [mewkiz/flac](https://github.com/mewkiz/flac)
* [hajimehoshi/go-mp3](https://github.com/hajimehoshi/go-mp3)
* [jfreymuth/oggvorbis](https://github.com/jfreymuth/oggvorbis)
* [tslocum/cview](https://gitlab.com/tslocum/cview)

## Documentation

See [CONFIGURATION.md](https://gitlab.com/tslocum/ditty/blob/master/CONFIGURATION.md) for default keybindings.

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/ditty/issues).
