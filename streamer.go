package main

import (
	"time"

	"github.com/faiface/beep"
)

type rateLimitedStreamer struct {
	streamer beep.Streamer
	rate     time.Duration
	last     time.Time
}

func newRateLimitedStreamer(streamer beep.Streamer, rate time.Duration) *rateLimitedStreamer {
	s := rateLimitedStreamer{streamer: streamer, rate: rate}
	return &s
}

func (s *rateLimitedStreamer) Stream(samples [][2]float64) (n int, ok bool) {
	if time.Since(s.last) < s.rate {
		time.Sleep(time.Until(s.last.Add(s.rate)))
	}
	s.last = time.Now()

	return s.streamer.Stream(samples)
}

func (s *rateLimitedStreamer) Err() error {
	return s.streamer.Err()
}
