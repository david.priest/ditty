module gitlab.com/tslocum/ditty

go 1.13

require (
	github.com/dhowden/tag v0.0.0-20191122115059-7e5c04feccd8
	github.com/faiface/beep v1.0.2
	github.com/gdamore/tcell v1.3.0
	github.com/hajimehoshi/go-mp3 v0.2.1 // indirect
	github.com/hajimehoshi/oto v0.5.4 // indirect
	github.com/jfreymuth/oggvorbis v1.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.8
	github.com/mewkiz/flac v1.0.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/tslocum/cbind v0.1.0
	gitlab.com/tslocum/cview v1.4.2-0.20200128151041-339db80f666d
	golang.org/x/exp v0.0.0-20200119233911-0405dc783f0a // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	golang.org/x/mobile v0.0.0-20200123024942-82c397c4c527 // indirect
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
