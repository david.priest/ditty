This document covers the [ditty](https://gitlab.com/tslocum/ditty)
configuration options and their defaults.

# Options
 
* **--buffer-size** Audio buffer size, defaults to 500ms.
* **--restrict-library** Restrict access to a folder and its subfolders.
* **--disable-autoplay** Disable automatically playing the queue.
* **--disable-mouse** Disable mouse support.
* **--fd** Write audio in WAV format to the specified file descriptor instead. This allows ditty to be used over ssh:
  * `ssh ditty.rocketnine.space -t 'ditty --fd=2' 2> >(aplay --quiet)`
  * Note: Writing to a file descriptor disables pause support.

# Default keybindings

* **Select** Enter
* **Pause** Space
* **Queue** Q
* **Delete from queue** D
* **Toggle focused list** Tab
* **Browse items** J/K, Down/Up and PageDown/PageUp
* **Previous track** P
* **Next track** N
* **Volume** -/+/M
* **Refresh** R
* **Toggle hidden folder visibility** .
* **Browse parent folder and focus last** Backspace
* **Exit** Ctrl+C or Alt+Q

# Default ~/.config/ditty/config.yaml

```yaml
volume: 75

input:
  select:
    - 'Enter'
  pause:
    - 'Space'
  refresh:
    - 'r'
  queue:
    - 'q'
  delete:
    - 'd'
  focus-next:
    - 'Tab'
  hidden-folders:
    - '.'
  browse-parent:
    - 'Backspace'
  volume-mute:
    - 'm'
  volume-down:
    - '-'
  volume-up:
    - '+'
  previous-item:
    - 'Up'
    - 'k'
  next-item:
    - 'Down'
    - 'j'
  previous-page:
    - 'PageUp'
  next-page:
    - 'PageDown'
  previous-track:
    - 'p'
  next-track:
    - 'n'
  exit:
    - 'Alt+q'
```
